#!/bin/bash

docker volume rm aimed_archives
docker volume rm aimed_results
docker network rm kafka-network
