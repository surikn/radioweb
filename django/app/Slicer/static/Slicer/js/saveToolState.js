function saveSliceToolState(researchID, sliceID, RectangleRoiState, EllipticalRoiState) {
    const url = `/series/save_toolstate/${researchID}/${sliceID}`;
    
    console.log("SAVING TOOLSTATE OF SLICE: ", sliceID);

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", token);
            }
        }
    });

    let token = document.getElementsByName("csrfmiddlewaretoken")[0].getAttribute("value");

    let toolsData = {
        "RectangleRoi": RectangleRoiState == undefined ? "" : RectangleRoiState.data,
        "EllipticalRoi": EllipticalRoiState == undefined ? "" : EllipticalRoiState.data,
    }

    console.log("Sending tooldata: ", toolsData)

    $.ajax({
        type: "POST",
        url: url,
        data: {
            tools_data: JSON.stringify(toolsData),
        },
        success: (function (response) {
            if (!response["ok"]) {
                M.toast({html: "Ошибка :("})
        	    console.log("ERROR: ", response);
            } else {
                M.toast({html: "Изменения сохранены"})
            }
        }),
        error: (function (response) {
        	console.log("ERROR: ", response);
        }),
    });
}