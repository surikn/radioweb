from django.contrib import admin
from .models import Research, ResearchTool

admin.site.register(Research)
admin.site.register(ResearchTool)
