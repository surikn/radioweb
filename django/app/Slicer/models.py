from django.db import models
from datetime import datetime
from django.conf import settings
from django.contrib.postgres.fields import JSONField
import random
import json
import os

def generate_hash(size=128):
    hash = random.getrandbits(128)
    while Research.objects.filter(bot_token=hash).count() > 0:
        hash = random.getrandbits(128)
    return hash
class Research(models.Model):
    study_date = models.DateField(auto_now=True)
    study_time = models.TimeField(auto_now=True)
    patient_id = models.CharField(max_length=128)
    series_instance_uid = models.CharField(max_length=128)
    series_number = models.CharField(max_length=128)
    dicom_tags = models.TextField()
    dicom_names = models.TextField()
    dir_name = models.CharField(max_length=256)
    zip_name = models.CharField(max_length=256)
    preview_image = models.CharField(max_length=128)
    predictions_dir = models.CharField(max_length=128)
    predictions_nods = models.TextField()
    bot_token = models.CharField(max_length=64)

    owner_id = models.IntegerField()

    upload_time = models.DateTimeField(auto_now=True)

    @property
    def dicom_names_list(self):
        return json.loads(self.dicom_names)

    @property
    def zip_path(self):
        return os.path.join(settings.BASE_DIR, "research_storage", "zips", self.zip_name)

    @property
    def dir_path(self):
        return os.path.join(settings.BASE_DIR, "research_storage", self.zip_name)

    def __str__(self):
        return self.series_instance_uid 

    def save(self, *args, **kwargs):
        self.bot_token = generate_hash()
        super().save(*args, **kwargs)

class ResearchTool(models.Model):
    editor_id = models.IntegerField()

    research_id = models.IntegerField()
    slice_id = models.IntegerField()

    rectangle_roi_tool = JSONField()
    elliptical_roi_tool = JSONField()

    change_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Research {} slice {} toolstate".format(self.research_id, self.slice_id)
