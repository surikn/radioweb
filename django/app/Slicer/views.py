from django.http import HttpRequest, HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from .slicer import handle_research
from .models import Research, ResearchTool
from Account.models import User, ExtendedUser
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.http import JsonResponse
from django.core import serializers
import json, zipfile
import requests
import os

def zipfolder(path, ziph):
    for root, dirs, files in os.walk(path):
        for f in files:
            print(os.path.join(root, f))
            ziph.write(os.path.join(root, f))

def research_list(request):
    researches = Research.objects.all()
    return render(request, "Slicer/research_list.html", {"researches": researches})

def upload_research(request):
    if "id" not in request.session:
        return HttpResponse("{'ok': false, 'error': 'you have not permissions for this operation'}", content_type="application/json")
    user_id = request.session["id"]
    if request.method == "POST" and "file" in request.FILES: 
        research = request.FILES["file"]
        resp = handle_research(research, user_id)
        return HttpResponse(json.dumps(resp), content_type="application/json")
    return HttpResponse("{'ok': false, 'error': 'invalid request'}", content_type="application/json")

def view_research(request, id):
    if "id" not in request.session:
        return HttpResponseRedirect("/")

    mode = "production"
    if "mode" in request.GET and request.GET["mode"] == "dev":
        mode = "development"

    user_id = request.session["id"]
    user = User.objects.get(id=user_id)
    ext_user = ExtendedUser.objects.get(userID=user_id)

    res = Research.objects.filter(id=id)
    if res.count() != 1:
        return HttpResponse("404")
    
    res = res[0]
    nods, dicom_tags = [], []
    if res.predictions_nods != "":
        nods = json.loads(res.predictions_nods)
    if res.dicom_tags != "":
        dicom_tags = json.loads(res.dicom_tags)
    return render(request, "Slicer/view_research.html", {
                "research": res,
                "extUser": ext_user,
                "user": user,
                "nods": nods,
                "dicom_tags": dicom_tags,
                "MODE": mode,
                "lang": "en",
            }
        )

def mark_up_research(request, id):
    if "id" not in request.session:
        return HttpResponseRedirect("/")

    mode = "production"
    if "mode" in request.GET and request.GET["mode"] == "dev":
        mode = "development"

    user_id = request.session["id"]
    user = User.objects.get(id=user_id)
    ext_user = ExtendedUser.objects.get(userID=user_id)

    res = Research.objects.filter(id=id)
    if res.count() != 1:
        return HttpResponse("404")

    research_tool_state_qs = ResearchTool.objects.filter(research_id=id, editor_id=user_id)
    research_tool_state = {}
    for slice_tool_state in list(research_tool_state_qs):
        research_tool_state[slice_tool_state.slice_id] = {
           "RectangleRoi": json.loads(slice_tool_state.rectangle_roi_tool),
           "EllipticalRoi": json.loads(slice_tool_state.elliptical_roi_tool),
        }
    research_tool_state_json = json.dumps(research_tool_state)

    res = res[0]
    nods, dicom_tags = [], []
    if res.predictions_nods != "":
        nods = json.loads(res.predictions_nods)
    if res.dicom_tags != "":
        dicom_tags = json.loads(res.dicom_tags)

    return render(request, "Slicer/mark_up_research.html", {
                "research": res,
                "extUser": ext_user,
                "user": user,
                "nods": nods,
                "dicom_tags": dicom_tags,
                "MODE": mode,
                "toolstate": research_tool_state_json,
                "lang": "en",
            }
        )

def save_tool_state(request, research_id, slice_id):
    if "id" not in request.session:
        return HttpResponseRedirect("/")

    if "tools_data" not in request.POST:
        return JsonResponse({"ok": False, "error": "Invalid request"})

    user_id = request.session["id"]
    db_tools_data = ResearchTool.objects.filter(editor_id=user_id, research_id=research_id, slice_id=slice_id)

    tools_data = json.loads(request.POST["tools_data"])

    print("TOOLS DATA: ", tools_data)

    if db_tools_data.count() == 0:
        db_tools_data = ResearchTool.objects.create(editor_id=user_id, research_id=research_id, slice_id=slice_id, 
        rectangle_roi_tool=tools_data.get("RectangleRoi", []), elliptical_roi_tool=tools_data.get("EllipticalRoi", []))
        db_tools_data.save()
    else:
        db_tools_data = db_tools_data[0]

        db_tools_data.rectangle_roi_tool = tools_data.get("RectangleRoi", []) # json.dumps(tools_data.get("RectangleRoi", ""))
        db_tools_data.elliptical_roi_tool = tools_data.get("EllipticalRoi", []) # json.dumps(tools_data.get("EllipticalRoi", ""))
        db_tools_data.save()

    return JsonResponse({"ok": True, "error": None})


@csrf_exempt
def kafka_processed(request):
    if request.method == "POST" and "data" in request.POST:
        msg = json.loads(request.POST["data"])
       
        print("kafka processed")

        if msg["code"] == "success":
            path = msg["path"]
            research_id = int(msg["id"])

            research = Research.objects.filter(id=research_id)

            if research.count() != 1:
                print("Invalid research id recieved from kafka!")
                return HttpResponse("Invalid research id recieved from kafka!")

            dir_path = os.path.join("static/research_storage/results/experiments", path, "_")
            zipf = zipfile.ZipFile(f"static/research_storage/results/zips/{path}.zip", "w", zipfile.ZIP_DEFLATED)
            zipfolder(dir_path, zipf)
            zipf.close()

            research = research[0]
            research.predictions_dir = path
            research.predictions_nods = json.dumps(msg["nods"])
            research.save()

            # bot_response = requests.put("http://auth:8082/research", data={
            #                     "Token": research.bot_token,
            #                     "Path": settings.HOSTNAME + "/static/research_storage/dicoms/" + research.zip_name, 
            #                     "Slices": msg["nods"],
            #                 }
            #             )
            # print("Bot response: ", bot_response.text)
        else:
            print("An error occured during the prediction!!!")

        return HttpResponse("OK")
    return HttpResponse("Invalid request")
