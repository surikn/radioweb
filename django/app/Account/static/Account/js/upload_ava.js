function uploadAva() {
    const url = '/home/uploadAva';
    let token = document.getElementsByName("csrfmiddlewaretoken")[0].getAttribute("value")

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    // span: ${ INTERFACE_LANG == "ru" ? "Загрузите файл" : "Upload image" }
    // placeholder: ${ INTERFACE_LANG == "en" ? "Upload" : "Загрузить" }
    let uploadAvaForm = `<form id = "uploadAvaForm" action="/home/uploadAva" enctype="multipart/form-data">
        <div class="file-field input-field">
            <div class="btn">
                <span> Upload image  </span>
                <input type="file" name="file" id="file">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Upload">
            </div>
        </div>
    </form>`;



    Swal.fire({
        title: INTERFACE_LANG == "ru" ? 'Загрузить фотографию' : "Upload image",
        html: uploadAvaForm,
        confirmButtonText: INTERFACE_LANG == "ru" ? "Отправить" : "Send",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {
            var send = new Promise((resolve, reject) => {
                let form = document.getElementById('uploadAvaForm');
                let formData = new FormData(form);

                if (file.value.length == 0) {
                    reject(INTERFACE_LANG == "ru" ? "Вы не прикрепили файл" : "You did not attach the file");
                }

                formData.append('file', file);
                
                let xhr = new XMLHttpRequest();
                xhr.open('POST', form.getAttribute('action'), true);
                xhr.setRequestHeader("X-CSRFToken", token);

                xhr.responseType = 'json';
                xhr.onload = function () {
                    if (xhr.readyState === xhr.DONE) {
                        if (xhr.status === 200) {
                            if (!xhr.response.ok) {
                                reject(xhr.response.msg)
                            } 
                        }
                    }
                };

                xhr.send(formData);
            }).then(value => {
                console.log(value)
            }, reason => {
                Swal.showValidationMessage (
                    `Ошибка: ${reason}`
                )
            })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            Swal.fire({
                type: 'success',
                title: INTERFACE_LANG == "ru" ? 'Файл был успешно загружен!' : "File was uploaded successfully",
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                window.location = "/home";
            });
        }
    })
}