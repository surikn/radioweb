let uploadContainer = $("#uploadResearchContainer");
let loc = window.location;

function renderLoadingResearche(status, progress=-1) {
	if (uploadContainer.html() == "") {
		let htmlContent = `
        <div id = "progressUploadingResearch">
        	<p style="statusText" id = "statusText"><b>` + INTERFACE_LANG == "ru" ? "Статус: " : "Status: " + `</b> ${status}</p>
        </div>
        <div class="progress">
      	`
      	if (progress == -1) {
      		htmlContent += `<div id = "progressBar" class="indeterminate"></div>`;
      	} else {
      		htmlContent += `<div id = "progressBar" class="determinate" style="width: ${progress}%"></div>`;
      	}
      	htmlContent += `</div>`;

		uploadContainer.html(htmlContent);
	} else {
		$("#progressUploadingResearch").html(`<p><b>` + INTERFACE_LANG == "ru" ? "Статус: " : "Status: " + `</b> ${status}</p>`);
		if (progress == -1) {
			$("#progressBar").removeClass("determinate");
			$("#progressBar").addClass("indeterminate");
		} else {
			$("#progressBar").removeClass("indeterminate");
			$("#progressBar").addClass("determinate");
			$("#progressBar").css("width", `${progress}%`)
		}
	}
}


function uploadResearch() {
    let token = document.getElementsByName("csrfmiddlewaretoken")[0].getAttribute("value")

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    // span: ${ INTERFACE_LANG == "ru" ? "Загрузите архив" : "Upload archive" }
    // placeholder: ${ INTERFACE_LANG == "ru" ? "Загрузить" : "Upload" }
    let uploadAvaForm = `<form id = "uploadResearchForm" action="/series/upload_research" enctype="multipart/form-data">
        <div class="file-field input-field">
            <div class="btn">
                <span>Upload archive</span>
                <input type="file" name="file" id="file">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Upload">
            </div>
        </div>
    </form>`;

    Swal.fire({
        title: INTERFACE_LANG == "ru" ? "Загрузить исследование" : "Upload research",
        html: uploadAvaForm,
        confirmButtonText: INTERFACE_LANG == "ru" ? "Загрузить" : "Upload",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {
            var send = new Promise((resolve, reject) => {
                let form = document.getElementById('uploadResearchForm');
                let formData = new FormData(form);

                if (file.value.length == 0) {
                    reject("Вы не прикрепили файл")
                }
				renderLoadingResearche(INTERFACE_LANG == "ru" ? "Загрузка файла на сервер" : "Uploading file on server", -1);
                formData.append('file', file);
                
                let xhr = new XMLHttpRequest();
                xhr.open('POST', form.getAttribute('action'), true);
                xhr.setRequestHeader("X-CSRFToken", token);

				xhr.upload.addEventListener("progress", function(evt){
					if (evt.lengthComputable) {
						var percentComplete = (evt.loaded / evt.total) * 100;
						renderLoadingResearche(INTERFACE_LANG == "ru" ? "Загрузка файла на сервер" : "Uploading file on server", percentComplete);
						if (percentComplete == 100) {
							renderLoadingResearche(INTERFACE_LANG == "ru" ? "Обработка исследования..." : "Research processing...");
						}
					}
			   }, false);

			   xhr.onreadystatechange = function() {
					if(xhr.readyState === 4 && xhr.status === 200) {
                        let resp = xhr.response;
						if (resp.ok) {
							window.location = "/home/view";
						} else {
							Swal.fire({
								type: 'error',
								title: 'Ошибка',
								text: resp.error,
							});
							uploadContainer.html(" ");
						}
				  	}
				}

                xhr.responseType = 'json';
				xhr.send(formData);
            }).then(value => {
                console.log("VALUE: ", value)
            }, reason => {
                Swal.showValidationMessage (
                    `Ошибка: ${reason}`
                )
            })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
       
    })
}