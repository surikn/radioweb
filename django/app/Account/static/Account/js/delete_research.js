function makeRequest(research_id) {
  console.log("RESEARCH ID: ", research_id);
  const url = "/home/deleteResearch";
  let token = document.getElementsByName("csrfmiddlewaretoken")[0].getAttribute("value")

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", token);
            }
        },
    });

    let prom = new Promise(function(resolve, reject) {
      $.ajax({
        type: "POST",
        url: url,
        data: {
          research_id: research_id,
        },
        success: (function(response) {
          console.log("RESPONSE: ", response);
          if (!response["ok"]) {
            reject(response);
          }
          resolve(response);
          // return true;
        }),
        error: function() {
          reject(response);
          // return false;
        },
      });
    });

    return prom;
}

function deleteResearch(research_id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          makeRequest(research_id).then(function(response) {
            Swal.fire(
              'Deleted!',
              'Research has been deleted.',
              'success'
            )
            setTimeout(function() {
              location.reload();
            }, 2000);  
          }, function(response) {
            Swal.fire(
              'Error!',
              'Something gones wrong. Try again!',
              'error',
            )
          });
        }
      })
}